const { authJwt } = require("../middlewares");
const controller = require("../controllers/db.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  
  app.get(
    "/api/db/attrs/:table_name",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getAttrs
  );

  app.get(
    "/api/db/params/:table_name",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getDataParams
  );
  
  app.get(
    "/api/db/:table_name",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getAllData
  );

  app.post(
    "/api/db/:table_name",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.insertData
  );

  app.patch(
    "/api/db/:table_name/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.updateData
  );

  app.delete(
    "/api/db/:table_name/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.deleteData
  );

  app.get(
    "/api/db/:table_name/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getData
  );
};