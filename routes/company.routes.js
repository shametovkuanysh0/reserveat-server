const { authJwt } = require("../middlewares");
const controller = require("../controllers/company.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get(
    "/api/company/all",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getCompanies
  );
  
  app.get(
    "/api/company/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getCompany
  );

  app.post(
    "/api/company/",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.createCompany
  );

  app.put(
    "/api/company/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.updateCompany
  );

  app.delete(
    "/api/company/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.deleteCompany
  );
};