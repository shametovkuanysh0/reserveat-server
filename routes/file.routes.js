const express = require("express");
const router = express.Router();
const fileController = require("../controllers/file.controller");

let routes = app => {
  router.post("/upload/:table_name/:id", fileController.uploadFiles);
  router.get("/files", fileController.getListFiles);
  router.get("/files/:name", fileController.download);
  router.get("/files/:table_name/:id", fileController.getFilesOfTable);

  return app.use("/", router);
};

module.exports = routes;