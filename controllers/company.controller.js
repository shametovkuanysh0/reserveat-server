const config = require("../config/auth.config");
const db = require("../models");
const Company = db.company;

exports.createCompany = (req, res) => {
    const company = new Company({
        name: req.body.name,
        email: req.body.email,
        description: req.body.description
    });

    company.save((err, company) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }
        res.send({ message: "Company was created successfully!" });
    });
};

exports.updateCompany = (req, res) => {
    Company.findOne({
        _id: req.params.id
    })
        .exec((err, company) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!company) {
            return res.status(404).send({ message: "Company Not found." });
        }
        company.name = req.body.name
        company.email = req.body.email
        company.description = req.body.description
        company.save((err, company) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
            res.send({ message: "Company was updated successfully!" });
        });
    });
};

exports.deleteCompany = (req, res) => {
    Company.deleteOne({
        _id: req.params.id
    })
        .exec((err, company) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!company) {
            return res.status(404).send({ message: "Company Not found." });
        }
        res.send({ message: "Company was deleted successfully!" });
    });
};

exports.getCompany = (req, res) => {
    Company.findOne({
        _id: req.params.id
    })
        .exec((err, company) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!company) {
            return res.status(404).send({ message: "Company Not found." });
        }
        res.status(200).send(company);
    });
};

exports.getCompanies = (req, res) => {
    Company.find()
        .exec((err, companies) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!companies) {
            return res.status(404).send({ message: "Company Not found." });
        }
        res.status(200).send(companies);
    });
};