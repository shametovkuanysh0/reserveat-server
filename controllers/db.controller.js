const config = require("../config/auth.config");
const db = require("../models");

exports.insertData = (req, res) => {
    const insertValues = req.body 
    const data = new (db[`${req.params.table_name}`])(insertValues);

    data.save((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }
        res.send({ message: "Insert went successfully!", result: result });
    });
};

exports.updateData = (req, res) => {
    db[`${req.params.table_name}`].updateOne({
        _id: req.params.id
    }, { $set: req.body })
        .exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!result) {
            return res.status(404).send({ message: "Not found." });
        }
        
        res.send({ message: "Data was updated successfully!" });
    });
};

exports.deleteData = (req, res) => {
    db[`${req.params.table_name}`].deleteOne({
        _id: req.params.id
    })
        .exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!result) {
            return res.status(404).send({ message: " Not found." });
        }
        
        res.send({ message: "Data was deleted successfully!" });
    });
};

exports.getAttrs = (req, res) => {
    res.status(200).send(db[`${req.params.table_name}`].getAttrs())
};

exports.getData = (req, res) => {
    db[`${req.params.table_name}`].findOne({
        _id: req.params.id
    })
        .exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!result) {
            return res.status(404).send({ message: " Not found." });
        }
        res.status(200).send({data: result, cols: db[`${req.params.table_name}`].getAttrs()});
    });
};

exports.getDataParams = (req, res) => {
    db[`${req.params.table_name}`].find(req.body.params)
        .exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!result) {
            return res.status(404).send({ message: " Not found." });
        }
        res.status(200).send({data: result, cols: db[`${req.params.table_name}`].getAttrs()});
    });
};

exports.getAllData = (req, res) => {
    (db[`${req.params.table_name}`]).takeAllData()
    
        .exec((err, data) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!data) {
            return res.status(404).send({ message: "Data Not found." });
        }
        res.status(200).send({data: data, cols: db[`${req.params.table_name}`].getAttrs()});
    });
};


// Object.keys(db.teams.findOne())