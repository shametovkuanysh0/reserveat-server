const db = require("../models");
const User = db.user;

exports.getAllUsers = (req, res) => {
    User.find()
        .populate("roles", "-__v")
        .exec((err, users) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }

        if (!users) {
            return res.status(404).send({ message: "Users Not found." });
        }

        res.status(200).send(users);
    });
};

exports.updateUser = (req, res) => {
    User.findById(req.params.id)
        .exec((err, user) => {
            if (err) {
                res.status(500).send({message: err})
                return
            }

            if (!user){
                return res.status(404).send({message: "User not found"})
            }

            user.company = req.body.company
            user.first_name = req.body.first_name
            user.last_name = req.body.last_name
            user.email = req.body.email
            user.roles = req.body.roles
            
            user.save((err, user) => {
                if (err) {
                    res.status(500).send({ message: err });
                    return;
                }
                res.send({ message: "User was updated successfully!" });
            });
        }) 
}