const mongoose = require("mongoose");

const Role = mongoose.model(
  "Role",
  new mongoose.Schema({
    name: String
  })
);
Role.getAttrs = function(){
  return [{name: "name", data_type:"text", required: true, type: "text", header: "Наименование"}]
}
Role.takeAllData = function(){
  return Role.find()
}
module.exports = Role;