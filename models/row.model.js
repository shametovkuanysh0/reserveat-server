const mongoose = require("mongoose");

const Row = mongoose.model(
  "Row",
  new mongoose.Schema({
    name: String,
    number: Number,
    seat_num: Number,
    room: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Room"
    }
  })
);
Row.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "number", data_type:"number", required: true, type: "number", header: "Номер", show: true},
            {name: "seat_num", data_type:"number", required: true, type: "number", header: "Количество мест", show: true},
            {name: "room", table_name:"room", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Зал", show: true},]
}
Row.takeAllData = function(){
  return Row.find()
}
module.exports = Row;