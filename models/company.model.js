const mongoose = require("mongoose");

const Company = mongoose.model(
  "Company",
  new mongoose.Schema({
    name: String,
    email: String,
    description: String
  })
);
Company.getAttrs = function(){
  return [{name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
          {name: "description", data_type:"text", required: true, type: "text", header: "Описание", show: true},
          {name: "email", data_type:"text", required: true, type: "email", header: "Почта", show: true}]
}
Company.takeAllData = function(){
  return Company.find()
}
module.exports = Company;