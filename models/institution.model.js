const mongoose = require("mongoose");

const Institution = mongoose.model(
  "Institution",
  new mongoose.Schema({
    name: String,
    description: String,
    cost: Number,
    images: [String],
    time: String,
    address: String,
    institution_type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "InstitutionType"
    },
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company"
  }
  })
);
Institution.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "description", data_type:"text", required: true, type: "text", header: "Описание", show: true},
            {name: "cost", data_type:"number", required: false, type: "number", header: "Средний чек", show: true},
            {name: "images", data_type:"files", required: false, type: "images", header: "Изображения", show: true},
            {name: "time", data_type:"string", required: false, type: "date-range", header: "Открытые часы", show: true},
            {name: "address", data_type:"text", required: true, type: "text", header: "Адрес", show: true},
            {name: "institution_type", table_name:"institutiontype", data_type:"object", required: true, type: "object", fieldToShow: "name", header: "Тип заведения", show: true},
            {name: "company", table_name:"company", data_type:"object", required: true, type: "object", fieldToShow: "name", header: "Компания", show: false},]
}
Institution.takeAllData = function(){
  return Institution.find()
  .populate("institutiontype", "-__v")
  .populate("company", "-__v")
}
module.exports = Institution;