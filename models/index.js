const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.company = require("./company.model");
db.institution = require("./institution.model");
db.meal = require("./meal.model");
db.institutiontype = require("./institution-type.model");
db.room = require("./room.model");
db.row = require("./row.model");
db.seat = require("./seat.model");
db.table = require("./table.model");
db.order = require("./order.model");
db.orderstatus = require("./order-status.model");

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;