const mongoose = require("mongoose");

const Table = mongoose.model(
  "Table",
  new mongoose.Schema({
    name: String,
    description: String,
    cost: Number,
    image: String,
    seat_num: Number,
    table_num: Number,
    institution: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Institution"
    }
  })
);
Table.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "description", data_type:"text", required: true, type: "text", header: "Описание", show: true},
            {name: "cost", data_type:"number", required: true, type: "number", header: "Цена", show: true},
            {name: "image", data_type:"image", required: true, type: "image", header: "Изображение", show: true},
            {name: "seat_num", data_type:"number", required: true, type: "number", header: "Количество мест", show: true},
            {name: "table_num", data_type:"number", required: true, type: "number", header: "Номер стола", show: true},
            {name: "institution", table_name:"institution", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Заведение", show: true},]
}
Table.takeAllData = function(){
  return Table.find()
}
module.exports = Table;