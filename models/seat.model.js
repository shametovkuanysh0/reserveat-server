const mongoose = require("mongoose");

const Seat = mongoose.model(
  "Seat",
  new mongoose.Schema({
    name: String,
    number: Number,
    row: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Row"
    }
  })
);
Seat.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "number", data_type:"number", required: true, type: "number", header: "Номер", show: true},
            {name: "row", table_name:"row", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Ряд", show: true},]
}
Seat.takeAllData = function(){
  return Seat.find()
}
module.exports = Seat;