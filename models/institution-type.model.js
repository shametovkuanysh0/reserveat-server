const mongoose = require("mongoose");

const InstitutionType = mongoose.model(
  "InstitutionType",
  new mongoose.Schema({
    name: String,
  })
);
InstitutionType.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},]
}
InstitutionType.takeAllData = function(){
  return InstitutionType.find()
}
module.exports = InstitutionType;