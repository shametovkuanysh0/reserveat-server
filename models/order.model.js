const mongoose = require("mongoose");

const Order = mongoose.model(
  "Order",
  new mongoose.Schema({
    name: String,
    total_cost: Number,
    date: Date,
    institution: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Institution"
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    table: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Table"
    }],
    seat: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Seat"
    }],
    meal: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Meal"
    }],
    status: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Orderstatus"
    }
    })
);
Order.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "total_cost", data_type:"text", required: true, type: "text", header: "Общая сумма", show: true},
            {name: "date", data_type:"date", required: true, type: "date", header: "Дата заказа", show: true},
            {name: "institution", table_name:"institution", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Заведение", show: true},
            {name: "user", table_name:"user", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Пользователь", show: true},
            {name: "table", table_name:"table", data_type:"array", required: false, type: "array", fieldToShow: "name", header: "Стол", show: true},
            {name: "seat", table_name:"seat", data_type:"array", required: false, type: "array", fieldToShow: "name", header: "Места", show: true},
            {name: "meal", table_name:"meal", data_type:"array", required: false, type: "array", fieldToShow: "name", header: "Еда", show: true},
            {name: "status", table_name:"orderstatus", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Статус", show: true},]
}
Order.takeAllData = function(){
  return Order.find()
}
module.exports = Order;