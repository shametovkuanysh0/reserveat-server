const mongoose = require("mongoose");

const Orderstatus = mongoose.model(
  "Orderstatus",
  new mongoose.Schema({
    name: String,
  })
);
Orderstatus.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},]
}
Orderstatus.takeAllData = function(){
  return Orderstatus.find()
}
module.exports = Orderstatus;