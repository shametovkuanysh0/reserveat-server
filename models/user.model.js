const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    login: String,
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
      }
    ],
    company: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Company"
    }
  })
);

User.getAttrs = function(){
  return [{name: "login", data_type:"text", required: true, type: "text", header: "Логин", show: true},
          {name: "first_name", data_type:"text", required: true, type: "text", header: "Имя", show: true},
          {name: "last_name", data_type:"text", required: true, type: "text", header: "Фамилия", show: true},
          {name: "email", data_type:"text", required: true, type: "email", header: "Почта", show: true},
          {name: "password", data_type:"text", required: true, type: "password", header: "Пароль", show: false},
          {name: "roles", table_name:"role", data_type:"array", required: false, type: "array", fieldToShow: "name", header: "Роли", show: true},
          {name: "company", table_name:"company", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Компания", show: true}]
}
User.takeAllData = function(){
  return User.find()
  .populate("roles", "-__v")
  .populate("company", "-__v")
}

module.exports = User;