const mongoose = require("mongoose");

const Meal = mongoose.model(
  "Meal",
  new mongoose.Schema({
    name: String,
    description: String,
    cost: Number,
    image: [String],
    time: Number,
    institution: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Institution"
    }]
  })
);
Meal.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "description", data_type:"text", required: true, type: "text", header: "Описание", show: true},
            {name: "image", data_type: "files", required: false, header: "Фото", show:true},
            {name: "cost", data_type:"number", required: true, type: "number", header: "Цена", show: true},
            {name: "time", data_type:"number", required: true, type: "number", header: "Время приготовления", show: true},
            {name: "institution", table_name:"institution", data_type:"array", required: false, type: "array", fieldToShow: "name", header: "Заведение", show: false},]
}
Meal.takeAllData = function(){
  return Meal.find().populate('institution',"-__v")
}
module.exports = Meal;