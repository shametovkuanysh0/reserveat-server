const mongoose = require("mongoose");

const Room = mongoose.model(
  "Room",
  new mongoose.Schema({
    name: String,
    description: String,
    row_num: Number,
    institution: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Institution"
    }
  })
);
Room.getAttrs = function(){
  return [  {name: "name", data_type:"text", required: true, type: "text", header: "Наименование", show: true},
            {name: "description", data_type:"text", required: true, type: "text", header: "Описание", show: true},
            {name: "row_num", data_type:"number", required: true, type: "number", header: "Количество рядов", show: true},
            {name: "institution", table_name:"institution", data_type:"object", required: false, type: "object", fieldToShow: "name", header: "Заведение", show: true},]
}
Room.takeAllData = function(){
  return Room.find()
}
module.exports = Room;